package get.tunedin.fragments;

import get.tunedin.FeedAdapter;
import get.tunedin.R;
import get.tunedin.event.BusProvider;
import get.tunedin.event.CacheEvent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.origamilabs.library.views.StaggeredGridView;
import com.squareup.otto.Subscribe;

public class FeedFragment extends Fragment {

	private FeedAdapter adapter;
	private ProgressBar progress;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {	
		View view = inflater.inflate(R.layout.feed_frame, null);
		progress = (ProgressBar) view.findViewById(R.id.progress);
		StaggeredGridView gridView = (StaggeredGridView) view.findViewById(R.id.staggeredGridView1);

    	int margin = getResources().getDimensionPixelSize(R.dimen.margin);

    	gridView.setItemMargin(margin); // set the GridView margin
    	
    	gridView.setPadding(margin, 0, margin, 0); // have the margin on the sides as well

    	adapter = new FeedAdapter(getActivity());

    	gridView.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
		return view;//gridView;
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
	}
	
	@Override public void onResume() {
		BusProvider.getInstance().register(this); 
		super.onResume();
	}

	@Override public void onPause() {	     
		BusProvider.getInstance().unregister(this); 
	    super.onPause(); 
	}
	
	@Subscribe public void onCacheChanged(CacheEvent event) {		
		if(progress.isShown())
			progress.setVisibility(View.GONE);
		else
			Toast.makeText(getActivity(), "Loading more", Toast.LENGTH_SHORT).show();
		getActivity().runOnUiThread(new Runnable() {			
			@Override
			public void run() {
				adapter.notifyDataSetChanged();
			}
		});
	}
}
