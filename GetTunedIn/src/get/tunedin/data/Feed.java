package get.tunedin.data;

public class Feed {
	
	public long created;
	public long id;
	public int tunedin; //the user watched this show
	public double rating;
	public int favorite;//the used added this show as favorite

	public Show show;
	public User user;
	
	public String type;
	public int age;

	@Override public String toString() {
		return "Feed [created=" + created + ", id=" + id + ", tunedin="
				+ tunedin + ", rating=" + rating + ", favorite=" + favorite
				+ ", show=" + show + ", user=" + user + ", type=" + type
				+ ", age=" + age + "]";
	}
	
}
