package get.tunedin.data;

public class User {

    public String id;
    public String username;
    public Avatar avatar;
      
    public boolean isFriend;
    
    public class Avatar {
    	public String w25;
    	public String w50;
    	public String w80;
    	public String w150;
    }
}
