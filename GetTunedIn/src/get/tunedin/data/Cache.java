package get.tunedin.data;

import get.tunedin.event.BusProvider;
import get.tunedin.event.CacheEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.squareup.otto.Produce;

public class Cache {


	private static List<Feed> feeds = Collections.synchronizedList(new ArrayList<Feed>());
	
	public static synchronized void addAll(List<Feed> list) {
		feeds.addAll(list);
		BusProvider.getInstance().post(produceCacheEvent(CacheEvent.Type.CREATE)); 
	}
	
	public static synchronized Feed get(int position) {
		//BusProvider.getInstance().post(produceCacheEvent(CacheEvent.Type.RETRIEVE)); 
		return feeds.get(position);
	}
	
	public static int count() {
		return feeds.size();
	}
	
	@Produce private static CacheEvent produceCacheEvent(CacheEvent.Type type) {		
		return new CacheEvent(type);
	}
}
