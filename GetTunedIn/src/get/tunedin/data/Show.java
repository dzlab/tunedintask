package get.tunedin.data;

public class Show {

	public long id;
	public long episode_id;
	public String title;
    public Image images;
     
    public class Image {

    	public Poster poster;
    	
    	public class Poster {
    		public String w120;
    		public String w180;
    		public String w380;
    	}
    }
}
