package get.tunedin;

import get.tunedin.data.Cache;
import get.tunedin.data.Feed;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class FeedAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater inflator;
	
	public FeedAdapter(Activity activity) {
		this.activity = activity;
		this.inflator = LayoutInflater.from(activity);		
		new FeedsTask().execute();
	}
	
	@Override
	public int getCount() {
		return Cache.count();
	}

	@Override
	public Object getItem(int position) {		
		return Cache.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {			
			convertView = inflator.inflate(R.layout.row_feed, null);
			holder = new ViewHolder();
			holder.showImage = (ImageView) convertView .findViewById(R.id.showImage);
			holder.showName  = (TextView) convertView .findViewById(R.id.showName);
			holder.userImage = (ImageView) convertView .findViewById(R.id.userImage);
			holder.userName  = (TextView) convertView .findViewById(R.id.userName);
			convertView.setTag(holder);
		}

		holder = (ViewHolder) convertView.getTag();
		Feed feed = (Feed) getItem(position);
		String showImageUrl = feed.show.images.poster.w120;		
		Picasso.with(activity).load(showImageUrl).into(holder.showImage);
		holder.showName.setText(feed.show.title);
		
		String userImageUrl = feed.user.avatar.w25;		
		Picasso.with(activity).load(userImageUrl).into(holder.userImage);
		holder.userName.setText(feed.user.username);
		
		if(position == getCount()-1) //load more feeds
			new FeedsTask().execute();
		return convertView;
	}

	static class ViewHolder {
		ImageView showImage;
		TextView showName;
		ImageView userImage;
		TextView userName;
	}
	
	
}
