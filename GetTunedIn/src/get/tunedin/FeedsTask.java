package get.tunedin;

import get.tunedin.data.Cache;
import get.tunedin.data.Feed;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.List;

import org.json.JSONObject;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.OkHttpClient;

public class FeedsTask extends AsyncTask<Void, Void, String> {

	public static String BASE_URL = "http://api.gettuned.in/activity/test";
    
	private static final OkHttpClient client = new OkHttpClient();
	private static final Gson sGson = new Gson();
    private static Meta meta;
    private static int offset = 0;
    
	@Override protected String doInBackground(Void... params) {
		String url = BASE_URL;
		if(meta != null) {
			if(meta.more==false) return null;
			offset += meta.submit;
			url += "?" + "cachekey=" + meta.cachekey + "&" + "offset=" + offset;
		}
		String result = null;
		try {
			result = doGet(url);		
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override protected void onPostExecute(String result) {
		if(result==null) return;
        try {
        	JSONObject obj = new JSONObject(result);
        	meta = sGson.fromJson(obj.getJSONObject("meta").toString(), Meta.class);        	
        	Type listType = new TypeToken<List<Feed>>(){}.getType();        	
			Cache.addAll((List<Feed>) sGson.fromJson(obj.getJSONArray("feed").toString(), listType));
			
        }catch(Exception e) {
        	e.printStackTrace();
        }
    }
	
	public class Meta {
		public String cachekey;
		public boolean more;
		public int total;
		public int submit;
	}
	
	private static String doGet(String url) throws Exception {
		String result = null;		
		HttpURLConnection connection = client.open(new URL(url));
		InputStream in = null;
		try {			
			// Read the response.
	        in = connection.getInputStream();
	        byte[] response = readFully(in);
	        result = new String(response, "UTF-8");
	      
		} finally {
	        if (in != null) in.close();	    
		}
		return result;		
	}
	private static byte[] readFully(InputStream in) throws IOException {
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    byte[] buffer = new byte[1024];
	    for (int count; (count = in.read(buffer)) != -1; ) {
	      out.write(buffer, 0, count);
	    }
	    return out.toByteArray();	  
	}
}
